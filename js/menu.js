const hamburger = document.getElementById("hamburger"),
      mainMenu = document.getElementById("main-menu"),
      closeButton = document.getElementById("close-button");

hamburger.addEventListener('click', ()=> {
    mainMenu.classList.add("show-menu");
});

closeButton.addEventListener('click', ()=> {
    mainMenu.classList.remove("show-menu");
});